/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeShop')
/*
 * Register modules
 */
.run(function ($widget) {

	$widget.newWidget({
		type: 'shopTrackOrder',
		title: 'Track orders',
		description: 'Tracking orders using secure id.',
		groups: ['commons'],
		icon: 'search',
		// help
		help: '',
		helpId: 'track-order',
		// functional (page)
		templateUrl: 'views/widgets/amh-shop-track-order.html',
		controller: 'AmhShopTrackOrderCtrl',
	});

	$widget.newWidget({
		type: 'registerForm',
		title: 'Register form',
		description: 'Register customer info when a service or product is selected.',
		groups: ['commons'],
		icon: 'search',
		// help
		help: '',
		helpId: 'register-form',
		// functional (page)
		templateUrl: 'views/widgets/amh-shop-register-form.html',
		controller: 'AmhShopRegisterFormCtrl',
		setting: ['register-order', 'common-features']
	});
});
