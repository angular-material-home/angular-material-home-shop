/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeShop')

	/**
	 * Load widgets
	 */
	.run(function ($settings) {
	    $settings.newPage({
		type: 'quick-access',
		label: 'Quick access',
		description: 'Set categories and subservices in tab view',
		icon: 'settings',
		templateUrl: 'views/settings/amh-shop-setting-quick-access.html'
	    });
	    $settings.newPage({
		type: 'register-order',
		label: 'Register form',
		description: 'Setting of register form',
		icon: 'settings',
		templateUrl: 'views/settings/amh-shop-setting-register-order.html',
		controllerAs: 'ctrl',
		/*
		 * @ngInject
		 */
		controller: function () {
		    this.init = function () {
			// load data from model
			this.orderTitle = this.getProperty('orderTitle');
		    };
		}
	    });
	});
