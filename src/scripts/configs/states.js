/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeShop')
/**
 * State of the shop management
 */
.config(function ($routeProvider) {
	$routeProvider//
	// Page of a category (** not in use)
	.when('/categories/:categoryId', {
		templateUrl : 'views/amh-category.html',
		controller : 'AmhShopCategoryCtrl'
	})//
	// Choose a deliver way (** in use)
	.when('/shop/deliver', {
		templateUrl : 'views/amh-deliver.html',
		controller : 'AmhShopDeliverCtrl'
	})//
	// Create new order (** in use)
	.when('/shop/order/register', {
		templateUrl : 'views/amh-register-order.html',
		controller : 'AmhShopRegisterCtrl',
		controllerAs: 'ctrl'
	})
	// Page of an order (** in use)
	.when('/shop/orders/:secureId', {
		templateUrl : 'views/amh-order.html',
		controller : 'AmhShopOrderCtrl', 
		controllerAs: 'ctrl'
	})//


	// List root categories (* works but not in use)
	.when('/categories', { 
		templateUrl : 'views/amh-category.html',
		controller : 'AmhShopCategoryCtrl'
	})//
	// Page of a product (* works but not in use)
	.when('/products/:productId', {
		templateUrl : 'views/amh-product.html',
		controller : 'AmhShopProductCtrl'
	})//
	// Page of a service (* works but not in use)
	.when('/services/:serviceId', {
		templateUrl : 'views/amh-service.html',
		controller : 'AmhShopServiceCtrl'
	});//

});
