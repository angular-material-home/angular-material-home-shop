/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('ngMaterialHomeShop')

	/**
	 * @ngdoc controller
	 * @name angular-material-home-shop.controller:AmhShopRegisterCtrl
	 * @description # OrderItemCtrl Controller of the angular-material-home-shop.
	 * 
	 */
	.controller('AmhShopRegisterCtrl', function ($scope, $shop, $q, $routeParams,
		$translate, $navigator) {

	    this.data = {};

	    this.loadServices = function (services) {
		// support old model
		if ($routeParams.serviceId) {
		    services.push({
			item_id: $routeParams.serviceId,
			item_type: 'service',
			count: '1'
		    });
		}

		// load list of services
		if ($routeParams.order) {
		    var order = JSON.parse($routeParams.order);
		    angular.forEach(order.services, function (item) {
			services.push({
			    item_id: item.id,
			    item_type: 'service',
			    count: item.count || 1
			});
		    });
		    angular.forEach(order.products, function (item) {
			services.push({
			    item_id: item.id,
			    item_type: 'product',
			    count: item.count || 1
			});
		    });
		    if (order.deliver) {
			services.push({
			    item_id: order.deliver.id,
			    item_type: 'delivery',
			    count: 1
			});
		    }
		}
		return services;
	    };

	    this.register = function (myOrder) {
		if (this.registeringOrder) {
		    return;
		}

		var services = [];
		this.loadServices(services);
		// TODO: maso, 2019:load products

		// Old model, delivery item
		if ($routeParams.deliverId) {
		    services.push({
			item_id: $routeParams.deliverId,
			item_type: 'delivery',
			count: '1'
		    });
		}

		var order = {
		};
		myOrder.title = 'SHOP ORDER';
		this.registeringOrder = true;
		var ctrl = this;
		return $shop.putOrder(myOrder)
			.then(function (newOrder) {
			    order = newOrder;
			    if (newOrder.secureId) {
				order.id = newOrder.secureId;
			    }
			    return ctrl.putItmes(order, services);
			})//
			.then(function () {
			    $navigator.openPage('shop/orders/' + order.secureId, {});
			})//
			.finally(function () {
			    ctrl.registeringOrder = false;
			});
	    };


	    this.putItmes = function (order, items) {
		// TODO: maso, 2019: check items and order
		var promList = [];
		angular.forEach(items, function (item) {
		    promList.push(order.putItem(item));
		});
		return $q.all(promList);
	    };

	});

