'use strict';

angular.module('ngMaterialHomeShop')
/**
 * @ngdoc controller
 * @name AmhShopTrackOrderCtrl
 * @description پیگیری درخواست ها با شناسه امن
 * 
 * این کنترلر ویجتی را کنترل می کند که کاربر با وارد کردن شناسه امن یک درخواست در آن قادر 
 * خواهد بود به طور مستقیم به صفحه ی آن درخواست برود
 * <pre><code>
 * 	/order/:secureId
 * </code></pre>
 * 
 */
.controller('AmhShopTrackOrderCtrl', function ($scope, $navigator) {

	function orderFinder(orderSecureId){
		return $navigator.openPage('/shop/orders/' + orderSecureId);
	}
	$scope.orderFinder = orderFinder;
});

