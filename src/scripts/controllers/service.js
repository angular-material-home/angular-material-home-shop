/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('ngMaterialHomeShop')

/**
 * @ngdoc controller
 * @name AmhShopServiceCtrl
 * @description A service of shop module
 */
.controller('AmhShopServiceCtrl', function ($scope, $shop, $navigator, $tenant, QueryParameter) {

    $scope.$watch('wbModel.categoryId', function () {
        if ($scope.wbModel.categoryId === null) {
            return;
        }
        $scope.categoryNotFound = false;
        $scope.serviceNotFound = false;
        $scope.categories = null;
        $scope.services = null;
        // load parent category:needs only for meta tags in view
        loadCategory($scope.wbModel.categoryId);
        if ($scope.working) {
            return false;
        }
        $scope.working = true;
        var pp = new QueryParameter();
        pp.setFilter('parent', $scope.wbModel.categoryId);
        return $shop.getCategories(pp)//
        .then(function (cats) {
            if (cats.items.length > 0) {
                $scope.categories = cats.items;
                $scope.currentNavItem = $scope.categories[0].id;
                loadServices($scope.categories[0]);
                $scope.working = false;
                $scope.error = 0;
            } else {
                $scope.categoryNotFound = true;
                $scope.working = false;
            }
        }, function () {
            $scope.categoryNotFound = true;
            $scope.working = false;
            return;
        });
    });

    function loadCategory(catId) {
     // Load parent category: Now, needs only for meta tags in view.
        if (catId === '0') {
            return;
        } else {
            $shop.getCategory(catId).then(function (cat) {
                category = cat;
                $scope.parentCategory = cat;
            });
        }
    }

    function loadServices(category) {
        $scope.services = null;
        $scope.parentOfService = category.name;
        $scope.selectedService = false;
        $scope.serviceNotFound = false;
        if ($scope.serviceLoading) {
            return false;
        }
        $scope.serviceLoading = true;

        // get currency before getting services. This is a trick.
        $saas.setting('local.currency').then(function (sett) {
            $scope.unit = sett.value;

            // getting services here
            category.getServices().then(function (services) {
                if (services.items.length > 0) {
                    $scope.services = services;
                } else {
                    $scope.serviceNotFound = true;
                }
                $scope.serviceLoading = false;
            });
        });
    }

    function submitToOrder(service) {// send an array of serivces id
        // to a new controller to form
        // an order.
        $navigator.openPage('shop/order/register', {
            servicesId : [ service.id ]
        });
    }

    $scope.loadServices = loadServices;
    $scope.submitToOrder = submitToOrder;
    $scope.error = 0;
    $scope.working = false;
});
