/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('ngMaterialHomeShop')

/**
 * @ngdoc controller
 * @name amhShop.controller:AmhShopDeliverCtrl
 * @description # AmhShopCategoryCtrl Controller of the amhShop
 */
.controller('AmhShopDeliverCtrl', function ($scope, $shop, QueryParameter,
		$routeParams, $translate, $navigator) {

	var ctrl = {
			loading: false
	};
	function loadDeliveries() {
		var pp = new QueryParameter();
		pp.setOrder('id', 'd');
		ctrl.loading = true;
		return $shop.getDelivers(pp)
		.then(function (res) {
			$scope.delivers = res.items;
		}, function () {
			alert($translate.instant('Failed to load delivers.'));
		})//
		.finally(function () {
			ctrl.loading = false;
		});
		;
	}

	function submitToOrder(deliver) {
		$navigator.openPage('shop/order/register/', {
			deliverId: deliver.id,
			serviceId: $routeParams.serviceId
		});
	}

	$scope.ctrl = ctrl;
	$scope.submitToOrder = submitToOrder;
	loadDeliveries();
});
