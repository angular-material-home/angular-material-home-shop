/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('ngMaterialHomeShop')

/**
 * @ngdoc controller
 * @name angular-material-home-shop.controller:AmhShopOrderCtrl
 * @description # AmhShopOrderCtrl Controller of the angular-material-home-shop.
 * 
 */
.controller('AmhShopOrderCtrl', function ($translate, $shop, $routeParams, ShopOrder, $navigator) {

	this.order = {};
	this.totalP = 0;
	this.paymentChecked = false;

	this.actionFlags = {
			pay: false
	};

	this.loadOrder = function () {
		if (this.loading) {
			return;
		}
		this.loading = true;
		var ctrl = this;
		$shop.getOrder($routeParams.secureId, {
			graphql: '{id, title, full_name, phone, email, address, description, state, order_items{title, price, count, item_type}, histories{action,description}, customer{id, profiles{first_name,last_name}},attachments{id,description,mime_type,file_name,file_size}}'
		})//
		//graphql: '{id,title,full_name,phone,email,address,description,state,order_items{title,price,count,item_type},histories{action,description},customer{id,profiles{first_name,last_name}},assignee{id,profiles{first_name,last_name}}}'
		.then(function (order) {
			ctrl.initialize(order);
		}, function () {
			alert($translate.instant('Failed to load the order.'));
		})//
		.finally(function () {
			ctrl.loading = false;
		});
	};

	this.initialize = function (order) {
		this.order = new ShopOrder(order);
		this.order.secureId = $routeParams.secureId;
		this.checkForPayment();
		this.possibleActions();
		this.totalPrice();
	};

	this.totalPrice = function () {
		for (var i = 0; i < this.order.order_items.length; i++) {
			this.totalP = this.totalP + this.order.order_items[i].price * this.order.order_items[i].count;
		}
	};

	this.pay = function (data) {
		// create receipt and send to bank receipt page.
		return this.order.putPayment(data);
	};

	this.checkForPayment = function () {
		var ctrl = this;
		this.order.getPayments()
		.then(function (payments) {
			ctrl.paymentChecked = true;
			if (payments.items[0] && payments.items[0].payRef) {
				ctrl.isPayed = true;
			}
		}, function (error) {
			alert($translate.instant(error));
		});
	};

	this.possibleActions = function () {
		var ctrl = this;
		this.order.getPossibleTransitions()
		.then(function (actions) {
			ctrl.actionsChecked = true;
			ctrl.initActionFlags(actions.items);
		});
	};

	this.initActionFlags = function (actions) {
		var ctrl = this;
		angular.forEach(actions, function (action) {
			if (action.id === 'pay') {
				ctrl.actionFlags.pay = true;
			}
		});
	};

	this.addAttachment = function () {
		var ctrl = this;
		$navigator.openDialog({
			templateUrl: 'views/dialogs/attachment-new.html',
			config: {
				_files: []
			}
		})//
		.then(function (files) {
			ctrl.attach(files);
		});
	};

	this.attach = function (files) {
		var file = files[0].lfFile;
		var ctrl = this;
		this.order.putAttachment({
			description: file.name
		})//
		.then(function (attachment) {
			attachment.order_id = $routeParams.secureId;
			return attachment.uploadContent(file);
		})//
		.then(function (attachment) {
			ctrl.order.attachments.push(attachment.data);
		});
	};

	this.deleteAttachment = function (attachment) {
		var ctrl = this;
		return this.order.deleteAttachment(attachment)
		.then(function (res) {
			for (var i = 0; i < ctrl.order.attachments.length; i++) {
				if (ctrl.order.attachments[i].id === attachment.id) {
					ctrl.order.attachments.splice(i, 1);
					break;
				}
			}
		}, function (error) {
			alert($translate.instant('Failed to delete attachment'));
		});
	};

	this.showImage = function (order, attachment) {
		$navigator.openDialog({
			templateUrl: 'views/dialogs/show-image.html',
			config: {
				order: order,
				attachment: attachment
			}
		});
	};

	this.loadOrder();
});
