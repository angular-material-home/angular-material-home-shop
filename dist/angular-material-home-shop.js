/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/*
 * 
 */
angular.module('ngMaterialHomeShop', [//
    'ngMaterialHome',//
    'ngMaterialHomeBank',//
    'seen-shop',
    'seen-bank'
]);
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeShop')
/**
 * State of the shop management
 */
.config(function ($routeProvider) {
	$routeProvider//
	// Page of a category (** not in use)
	.when('/categories/:categoryId', {
		templateUrl : 'views/amh-category.html',
		controller : 'AmhShopCategoryCtrl'
	})//
	// Choose a deliver way (** in use)
	.when('/shop/deliver', {
		templateUrl : 'views/amh-deliver.html',
		controller : 'AmhShopDeliverCtrl'
	})//
	// Create new order (** in use)
	.when('/shop/order/register', {
		templateUrl : 'views/amh-register-order.html',
		controller : 'AmhShopRegisterCtrl',
		controllerAs: 'ctrl'
	})
	// Page of an order (** in use)
	.when('/shop/orders/:secureId', {
		templateUrl : 'views/amh-order.html',
		controller : 'AmhShopOrderCtrl', 
		controllerAs: 'ctrl'
	})//


	// List root categories (* works but not in use)
	.when('/categories', { 
		templateUrl : 'views/amh-category.html',
		controller : 'AmhShopCategoryCtrl'
	})//
	// Page of a product (* works but not in use)
	.when('/products/:productId', {
		templateUrl : 'views/amh-product.html',
		controller : 'AmhShopProductCtrl'
	})//
	// Page of a service (* works but not in use)
	.when('/services/:serviceId', {
		templateUrl : 'views/amh-service.html',
		controller : 'AmhShopServiceCtrl'
	});//

});

/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('ngMaterialHomeShop')

/**
 * @ngdoc controller
 * @name amhShop.controller:AmhShopCategoryCtrl
 * @description # AmhShopCategoryCtrl Controller of the amhShop
 */
.controller('AmhShopCategoryCtrl', function ($scope, $rootScope, $shop, QueryParameter,
		$routeParams, $q, $translate, $navigator, $cms, $actions) {

	var ctrl = {};

	$scope.loadingCategories = false;
	$scope.loadingServices = false;
	$scope.categoryNothingFound = false;

	var catId = $routeParams.categoryId ? $routeParams.categoryId : '0';
	var category = null;

	var lastCategoriesPage = null;
	var lastServicesPage = null;

	$scope.categories = [];
	$scope.services = [];

	var pp = new QueryParameter();
	pp.setOrder('id', 'd');
	pp.setFilter('parent_id', catId);

	var servicesPP = new QueryParameter();
	servicesPP.setOrder('id', 'd');

	function loadNextCategories() {

		if ($scope.loadingCategories) {
			return;
		}
		if (lastCategoriesPage && !lastCategoriesPage.hasMore()) {
			return;
		}
		if (lastCategoriesPage) {
			pp.setPage(lastCategoriesPage.next());
		} else {
			pp.setPage(1);
		}
		$scope.loadingCategories = true;

		$shop.getCategories(pp)//
		.then(function (subCats) {
			if (subCats.length === 0) {
				$scope.categoryNothingFound = true;
			}
			$scope.categories = $scope.categories.concat(subCats.items);
			lastCategoriesPage = subCats;
			return subCats;
		}, function () {
			alert($translate.instant('Failed to get items.'));
		})
		.finally(function () {
			$scope.loadingCategories = false;
		});

	}

	function loadNextServices() {

		if (!category) {
			return;
		}
		if ($scope.loadingServices) {
			return;
		}
		if (lastServicesPage && !lastServicesPage.hasMore()) {
			return;
		}
		if (lastServicesPage) {
			servicesPP.setPage(lastServicesPage.next());
		} else {
			servicesPP.setPage(1);
		}
		$scope.loadingServices = true;

		// find services of a category
		category.getServices(servicesPP)//
		.then(function (servicesList) {
			$scope.services = $scope.services.concat(servicesList.items);
			lastServicesPage = servicesList;
			return;
		}, function () {
			alert($translate.instant('Failed to get items.'));
		})//
		.finally(function () {
			$scope.loadingServices = false;
		});
	}

	function goToCategoryPath(path) {
		$navigator.openPage(path);
	}

	function submitToOrder(service) {
		loadDeliveries()
		.then(function (res) {
			var delivers = res;
			if (!delivers || delivers.length === 0) {
				// go ditrectly to register form
				// send an array of serivces into a new controller to form an order.
				$navigator.openPage('shop/orders/new/', {serviceId: service.id});
			} else {
				// go to deliver page and select a way
				$navigator.openPage('shop/deliver/', {serviceId: service.id});
			}
		});
	}

	function loadDeliveries() {
		var pp = new QueryParameter();
		pp.setOrder('id', 'd');
		return $shop.getDelivers(pp)
		.then(function (res) {
			return res.items;
		});
	}

	function loadContentOfCategory() {
		if (catId === '0' || !category || !category.content || ctrl.loadingContent) {
			return;
		}
		ctrl.loadingContent = true;
		return $cms.getContent(category.content)//
		.then(function (nc) {
			$scope.content = nc;
			if (typeof nc !== 'undefined') {
				nc.value()//
				.then(function (val) {
					$scope.contentValue = val;
				});
			}
		})//
		.finally(function () {
			ctrl.loadingContent = false;
		});
	}

	/**
	 * Save or update content
	 * 
	 * @returns
	 */
	function saveContent() {
		var job = $scope.content ? _saveContent : _createContent;
		return job();
	}

	function _saveContent() {
		return $scope.content.setValue($scope.contentValue)//
		.then(function () {
			toast($translate.instant('Content is saved successfully'));
		}, function () {
			alert($translate.instant('Saving content is failed.'));
		});
	}

	function _createContent() {
		var contentName = 'amh-shop-category-content-' + category.id;
		var data = {
				name: contentName,
				title: 'Content of category ' + category.title + '[' + category.id + ']',
				description: 'Content created from AMH-Shop SPA.',
				mime_type: 'application/json',
				file_name: contentName + '.json'
		};
		$cms.putContent(data)//
		.then(function (myContent) {
			$scope.content = myContent;
			return $scope.content.setValue($scope.contentValue);
		}, function () {
			alert($translate.instant('Failed to create content.'));
		})//
		.then(function () {
			category.content = $scope.content.id;
			return category.update();
		})
		.then(function () {
			toast($translate.instant('Content is saved successfully'));
		}, function () {
			alert($translate.instant('Saving content is failed.'));
		});
	}

	function toggleEditable() {
		$scope.ngEditable = !$scope.ngEditable;
	}

	function load() {
		if (catId === '0') {
			loadNextCategories();
		} else {
			$shop.getCategory(catId)
			.then(function (cat) {
				category = cat;
				$scope.parentCategory = cat;
				return $q.all([loadContentOfCategory(), loadNextCategories(), loadNextServices()]);
			});
		}

	}

	/*
	 * Add scope menus
	 */
	$actions.newAction({// edit content
		id: 'amhsdp.scope.edit_content',
		priority: 15,
		icon: 'edit',
		label: 'Edit content',
		title: 'Edit content',
		description: 'Toggle edit mode of the current contet',
		visible: function () {
			return catId !== '0' && $rootScope.app.user.owner;
		},
		action: toggleEditable,
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});

	$actions.newAction({// add new content
		id: 'amhsdp.scope.new_content',
		priority: 15,
		icon: 'add_box',
		label: 'Create new content',
		title: 'Create new content',
		description: 'Create new content',
		visible: function () {
			return catId !== '0' && $rootScope.app.user.owner && (!ctrl.loadingContent && !$scope.content);
		},
		action: _createContent,
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});

	$actions.newAction({// save content
		id: 'amhsdp.scope.save_content',
		priority: 10,
		icon: 'save',
		label: 'Save current changes',
		title: 'Save current changes',
		description: 'Save current changes',
		visible: function () {
			return catId !== '0' && $rootScope.app.user.owner;
		},
		action: saveContent,
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});

	$scope.contentValue = {};
	$scope.ctrl = ctrl;
	$scope.submitToOrder = submitToOrder;
	$scope.goToCategoryPath = goToCategoryPath;
	$scope.nextCategories = loadNextCategories;
	$scope.loadNextServices = loadNextServices;
	$scope.load = load;
	$scope.parentCategory = category;

	load();
});

/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('ngMaterialHomeShop')

/**
 * @ngdoc controller
 * @name amhShop.controller:AmhShopDeliverCtrl
 * @description # AmhShopCategoryCtrl Controller of the amhShop
 */
.controller('AmhShopDeliverCtrl', function ($scope, $shop, QueryParameter,
		$routeParams, $translate, $navigator) {

	var ctrl = {
			loading: false
	};
	function loadDeliveries() {
		var pp = new QueryParameter();
		pp.setOrder('id', 'd');
		ctrl.loading = true;
		return $shop.getDelivers(pp)
		.then(function (res) {
			$scope.delivers = res.items;
		}, function () {
			alert($translate.instant('Failed to load delivers.'));
		})//
		.finally(function () {
			ctrl.loading = false;
		});
		;
	}

	function submitToOrder(deliver) {
		$navigator.openPage('shop/order/register/', {
			deliverId: deliver.id,
			serviceId: $routeParams.serviceId
		});
	}

	$scope.ctrl = ctrl;
	$scope.submitToOrder = submitToOrder;
	loadDeliveries();
});

/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('ngMaterialHomeShop')

/**
 * @ngdoc controller
 * @name angular-material-home-shop.controller:AmhShopOrderCtrl
 * @description # AmhShopOrderCtrl Controller of the angular-material-home-shop.
 * 
 */
.controller('AmhShopOrderCtrl', function ($translate, $shop, $routeParams, ShopOrder, $navigator) {

	this.order = {};
	this.totalP = 0;
	this.paymentChecked = false;

	this.actionFlags = {
			pay: false
	};

	this.loadOrder = function () {
		if (this.loading) {
			return;
		}
		this.loading = true;
		var ctrl = this;
		$shop.getOrder($routeParams.secureId, {
			graphql: '{id, title, full_name, phone, email, address, description, state, order_items{title, price, count, item_type}, histories{action,description}, customer{id, profiles{first_name,last_name}},attachments{id,description,mime_type,file_name,file_size}}'
		})//
		//graphql: '{id,title,full_name,phone,email,address,description,state,order_items{title,price,count,item_type},histories{action,description},customer{id,profiles{first_name,last_name}},assignee{id,profiles{first_name,last_name}}}'
		.then(function (order) {
			ctrl.initialize(order);
		}, function () {
			alert($translate.instant('Failed to load the order.'));
		})//
		.finally(function () {
			ctrl.loading = false;
		});
	};

	this.initialize = function (order) {
		this.order = new ShopOrder(order);
		this.order.secureId = $routeParams.secureId;
		this.checkForPayment();
		this.possibleActions();
		this.totalPrice();
	};

	this.totalPrice = function () {
		for (var i = 0; i < this.order.order_items.length; i++) {
			this.totalP = this.totalP + this.order.order_items[i].price * this.order.order_items[i].count;
		}
	};

	this.pay = function (data) {
		// create receipt and send to bank receipt page.
		return this.order.putPayment(data);
	};

	this.checkForPayment = function () {
		var ctrl = this;
		this.order.getPayments()
		.then(function (payments) {
			ctrl.paymentChecked = true;
			if (payments.items[0] && payments.items[0].payRef) {
				ctrl.isPayed = true;
			}
		}, function (error) {
			alert($translate.instant(error));
		});
	};

	this.possibleActions = function () {
		var ctrl = this;
		this.order.getPossibleTransitions()
		.then(function (actions) {
			ctrl.actionsChecked = true;
			ctrl.initActionFlags(actions.items);
		});
	};

	this.initActionFlags = function (actions) {
		var ctrl = this;
		angular.forEach(actions, function (action) {
			if (action.id === 'pay') {
				ctrl.actionFlags.pay = true;
			}
		});
	};

	this.addAttachment = function () {
		var ctrl = this;
		$navigator.openDialog({
			templateUrl: 'views/dialogs/attachment-new.html',
			config: {
				_files: []
			}
		})//
		.then(function (files) {
			ctrl.attach(files);
		});
	};

	this.attach = function (files) {
		var file = files[0].lfFile;
		var ctrl = this;
		this.order.putAttachment({
			description: file.name
		})//
		.then(function (attachment) {
			attachment.order_id = $routeParams.secureId;
			return attachment.uploadContent(file);
		})//
		.then(function (attachment) {
			ctrl.order.attachments.push(attachment.data);
		});
	};

	this.deleteAttachment = function (attachment) {
		var ctrl = this;
		return this.order.deleteAttachment(attachment)
		.then(function (res) {
			for (var i = 0; i < ctrl.order.attachments.length; i++) {
				if (ctrl.order.attachments[i].id === attachment.id) {
					ctrl.order.attachments.splice(i, 1);
					break;
				}
			}
		}, function (error) {
			alert($translate.instant('Failed to delete attachment'));
		});
	};

	this.showImage = function (order, attachment) {
		$navigator.openDialog({
			templateUrl: 'views/dialogs/show-image.html',
			config: {
				order: order,
				attachment: attachment
			}
		});
	};

	this.loadOrder();
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialHomeShop')

/**
 * @ngdoc controller
 * @name AmhShopProductCtrl
 * @description A product of shop module
 * 
 */
.controller('AmhShopProductCtrl', function() {
	
});



/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('ngMaterialHomeShop')

/**
 * @ngdoc controller
 * @name angular-material-home-shop.controller:AmhShopRegisterFormCtrl
 * @description # A controller to manage getting personal info when a service or product is selected
 * 
 */
.controller('AmhShopRegisterFormCtrl', function ($scope, $shop, $q, $routeParams, $navigator) {

	this.data = {};

	this.loadServices = function (services) {
		// support old model
		if ($routeParams.serviceId) {
			services.push({
				item_id: $routeParams.serviceId,
				item_type: 'service',
				count: '1'
			});
		}

		// load list of services
		if ($routeParams.order) {
			var order = JSON.parse($routeParams.order);
			angular.forEach(order.services, function (item) {
				services.push({
					item_id: item.id,
					item_type: 'service',
					count: item.count || 1
				});
			});
			angular.forEach(order.products, function (item) {
				services.push({
					item_id: item.id,
					item_type: 'product',
					count: item.count || 1
				});
			});
			if (order.deliver) {
				services.push({
					item_id: order.deliver.id,
					item_type: 'delivery',
					count: 1
				});
			}
		}
		return services;
	};

	this.register = function (myOrder) {
		if (this.registeringOrder) {
			return;
		}

		var services = [];
		this.loadServices(services);
		// TODO: maso, 2019:load products

		// Old model, delivery item
		if ($routeParams.deliverId) {
			services.push({
				item_id: $routeParams.deliverId,
				item_type: 'delivery',
				count: '1'
			});
		}

		myOrder.title = 'SHOP ORDER';
		this.registeringOrder = true;
		var ctrl = this;
		var secureId;
		return $shop.putOrder(myOrder)
		.then(function (newOrder) {
			secureId = newOrder.secureId;
			newOrder.id = secureId;
			return ctrl.putItmes(newOrder, services);
		})//
		.then(function (newOrder) {
			try {
				newOrder.title = Mustache.to_html($scope.wbModel.orderTitle, newOrder);
				newOrder.id = secureId;
			} catch (e) {
				//TODO: Masood, 2018: Do work if needed
			}
			//return newOrder.update();
		})//
		.then(function () {
			$navigator.openPage('shop/orders/' + secureId, {});
		})//
		.finally(function () {
			ctrl.registeringOrder = false;
		});
	};

	this.putItmes = function (order, items) {
		// TODO: maso, 2019: check items and order
		var promList = [];
		angular.forEach(items, function (item) {
			promList.push(order.putItem(item));
		});
		return $q.all(promList);
	};
});


/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('ngMaterialHomeShop')

	/**
	 * @ngdoc controller
	 * @name angular-material-home-shop.controller:AmhShopRegisterCtrl
	 * @description # OrderItemCtrl Controller of the angular-material-home-shop.
	 * 
	 */
	.controller('AmhShopRegisterCtrl', function ($scope, $shop, $q, $routeParams,
		$translate, $navigator) {

	    this.data = {};

	    this.loadServices = function (services) {
		// support old model
		if ($routeParams.serviceId) {
		    services.push({
			item_id: $routeParams.serviceId,
			item_type: 'service',
			count: '1'
		    });
		}

		// load list of services
		if ($routeParams.order) {
		    var order = JSON.parse($routeParams.order);
		    angular.forEach(order.services, function (item) {
			services.push({
			    item_id: item.id,
			    item_type: 'service',
			    count: item.count || 1
			});
		    });
		    angular.forEach(order.products, function (item) {
			services.push({
			    item_id: item.id,
			    item_type: 'product',
			    count: item.count || 1
			});
		    });
		    if (order.deliver) {
			services.push({
			    item_id: order.deliver.id,
			    item_type: 'delivery',
			    count: 1
			});
		    }
		}
		return services;
	    };

	    this.register = function (myOrder) {
		if (this.registeringOrder) {
		    return;
		}

		var services = [];
		this.loadServices(services);
		// TODO: maso, 2019:load products

		// Old model, delivery item
		if ($routeParams.deliverId) {
		    services.push({
			item_id: $routeParams.deliverId,
			item_type: 'delivery',
			count: '1'
		    });
		}

		var order = {
		};
		myOrder.title = 'SHOP ORDER';
		this.registeringOrder = true;
		var ctrl = this;
		return $shop.putOrder(myOrder)
			.then(function (newOrder) {
			    order = newOrder;
			    if (newOrder.secureId) {
				order.id = newOrder.secureId;
			    }
			    return ctrl.putItmes(order, services);
			})//
			.then(function () {
			    $navigator.openPage('shop/orders/' + order.secureId, {});
			})//
			.finally(function () {
			    ctrl.registeringOrder = false;
			});
	    };


	    this.putItmes = function (order, items) {
		// TODO: maso, 2019: check items and order
		var promList = [];
		angular.forEach(items, function (item) {
		    promList.push(order.putItem(item));
		});
		return $q.all(promList);
	    };

	});


/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('ngMaterialHomeShop')

/**
 * @ngdoc controller
 * @name AmhShopServiceCtrl
 * @description A service of shop module
 */
.controller('AmhShopServiceCtrl', function ($scope, $shop, $navigator, $tenant, QueryParameter) {

    $scope.$watch('wbModel.categoryId', function () {
        if ($scope.wbModel.categoryId === null) {
            return;
        }
        $scope.categoryNotFound = false;
        $scope.serviceNotFound = false;
        $scope.categories = null;
        $scope.services = null;
        // load parent category:needs only for meta tags in view
        loadCategory($scope.wbModel.categoryId);
        if ($scope.working) {
            return false;
        }
        $scope.working = true;
        var pp = new QueryParameter();
        pp.setFilter('parent', $scope.wbModel.categoryId);
        return $shop.getCategories(pp)//
        .then(function (cats) {
            if (cats.items.length > 0) {
                $scope.categories = cats.items;
                $scope.currentNavItem = $scope.categories[0].id;
                loadServices($scope.categories[0]);
                $scope.working = false;
                $scope.error = 0;
            } else {
                $scope.categoryNotFound = true;
                $scope.working = false;
            }
        }, function () {
            $scope.categoryNotFound = true;
            $scope.working = false;
            return;
        });
    });

    function loadCategory(catId) {
     // Load parent category: Now, needs only for meta tags in view.
        if (catId === '0') {
            return;
        } else {
            $shop.getCategory(catId).then(function (cat) {
                category = cat;
                $scope.parentCategory = cat;
            });
        }
    }

    function loadServices(category) {
        $scope.services = null;
        $scope.parentOfService = category.name;
        $scope.selectedService = false;
        $scope.serviceNotFound = false;
        if ($scope.serviceLoading) {
            return false;
        }
        $scope.serviceLoading = true;

        // get currency before getting services. This is a trick.
        $saas.setting('local.currency').then(function (sett) {
            $scope.unit = sett.value;

            // getting services here
            category.getServices().then(function (services) {
                if (services.items.length > 0) {
                    $scope.services = services;
                } else {
                    $scope.serviceNotFound = true;
                }
                $scope.serviceLoading = false;
            });
        });
    }

    function submitToOrder(service) {// send an array of serivces id
        // to a new controller to form
        // an order.
        $navigator.openPage('shop/order/register', {
            servicesId : [ service.id ]
        });
    }

    $scope.loadServices = loadServices;
    $scope.submitToOrder = submitToOrder;
    $scope.error = 0;
    $scope.working = false;
});

'use strict';

angular.module('ngMaterialHomeShop')
/**
 * @ngdoc controller
 * @name AmhShopTrackOrderCtrl
 * @description پیگیری درخواست ها با شناسه امن
 * 
 * این کنترلر ویجتی را کنترل می کند که کاربر با وارد کردن شناسه امن یک درخواست در آن قادر 
 * خواهد بود به طور مستقیم به صفحه ی آن درخواست برود
 * <pre><code>
 * 	/order/:secureId
 * </code></pre>
 * 
 */
.controller('AmhShopTrackOrderCtrl', function ($scope, $navigator) {

	function orderFinder(orderSecureId){
		return $navigator.openPage('/shop/orders/' + orderSecureId);
	}
	$scope.orderFinder = orderFinder;
});


/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeShop')

	/**
	 * Load widgets
	 */
	.run(function ($settings) {
	    $settings.newPage({
		type: 'quick-access',
		label: 'Quick access',
		description: 'Set categories and subservices in tab view',
		icon: 'settings',
		templateUrl: 'views/settings/amh-shop-setting-quick-access.html'
	    });
	    $settings.newPage({
		type: 'register-order',
		label: 'Register form',
		description: 'Setting of register form',
		icon: 'settings',
		templateUrl: 'views/settings/amh-shop-setting-register-order.html',
		controllerAs: 'ctrl',
		/*
		 * @ngInject
		 */
		controller: function () {
		    this.init = function () {
			// load data from model
			this.orderTitle = this.getProperty('orderTitle');
		    };
		}
	    });
	});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeShop')
/*
 * Register modules
 */
.run(function ($widget) {

	$widget.newWidget({
		type: 'shopTrackOrder',
		title: 'Track orders',
		description: 'Tracking orders using secure id.',
		groups: ['commons'],
		icon: 'search',
		// help
		help: '',
		helpId: 'track-order',
		// functional (page)
		templateUrl: 'views/widgets/amh-shop-track-order.html',
		controller: 'AmhShopTrackOrderCtrl',
	});

	$widget.newWidget({
		type: 'registerForm',
		title: 'Register form',
		description: 'Register customer info when a service or product is selected.',
		groups: ['commons'],
		icon: 'search',
		// help
		help: '',
		helpId: 'register-form',
		// functional (page)
		templateUrl: 'views/widgets/amh-shop-register-form.html',
		controller: 'AmhShopRegisterFormCtrl',
		setting: ['register-order', 'common-features']
	});
});

angular.module('ngMaterialHomeShop').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amh-category.html',
    "<md-content wb-infinate-scroll=loadNextServices flex>  <div layout=row layout-align=\"center center\" layout-wrap> <md-progress-circular md-mode=indeterminate md-diameter=96px ng-if=loadingCategories> </md-progress-circular> <div layout=column itemscope itemtype=http://schema.org/Thing md-colors=\"{borderColor: 'primary-200'}\" class=\"clickable-item md-whiteframe-2dp\" ng-repeat=\"category in categories\" layout-align=\"none center\" layout-margin ng-click=\"goToCategoryPath('categories/' + category.id)\" style=\"cursor: pointer\" layout-padding flex=30> <img itemprop=image ng-if=category.thumbnail style=\"max-height:128px; max-width:128px; min-height:128px; min-width: 128px\" ng-src=/api/cms/{{category.thumbnail}}/download alt={{category.name}}> <img ng-if=!category.thumbnail style=\"max-height:128px; max-width:128px; min-height:128px; min-width: 128px\" ng-src=images/default-folder.svg alt={{category.name}}> <p itemprop=name ng-href=categories/{{category.id}}>{{category.name}}</p> <meta ng-if=parentCategory.name itemprop=category content={{parentCategory.name}}> </div> </div>  <div layout=column>   <div ng-show=services layout=row layout-align=\"center center\" layout-wrap> <div itemscope itemtype=http://schema.org/Service class=\"clickable-item md-whiteframe-2dp\" md-colors=\"{borderColor: 'primary-200'}\" ng-repeat=\"service in services\" layout=column layout-align=\"none center\" layout-margin layout-padding ng-click=submitToOrder(service) style=\"cursor: pointer\" flex=30> <img itemprop=image ng-if=service.avatar style=\"max-height:100px; max-width:128px; min-height:128px; min-width: 128px\" ng-src={{service.avatar}} alt={{service.title}}> <img ng-if=!service.avatar style=\"max-height:128px; max-width:128px; min-height:128px; min-width: 128px\" ng-src=images/default-folder.svg alt={{service.title}}> <p itemprop=name ng-href=shop/create-order/{{service.id}}>{{service.title}}</p> <meta ng-if=parentCategory.name itemprop=category content={{parentCategory.name}}> </div> </div> </div>   </md-content>"
  );


  $templateCache.put('views/amh-deliver.html',
    " <div layout=row layout-align=\"center center\" layout-wrap flex> <md-progress-circular md-mode=indeterminate md-diameter=96px ng-if=ctrl.loading> </md-progress-circular> <div itemscope itemtype=http://schema.org/DeliveryMethod class=\"clickable-item md-whiteframe-2dp\" md-colors=\"{borderColor: 'primary-200'}\" ng-repeat=\"deliver in delivers\" layout=column layout-align=\"none center\" layout-margin ng-click=submitToOrder(deliver) style=\"cursor: pointer\" flex=20> <div> <img itemprop=image ng-if=deliver.avatar style=\"max-height:100px; max-width:128px; min-height:128px; min-width: 128px\" ng-src={{deliver.avatar}} alt={{deliver.title}}> <img ng-if=!deliver.avatar style=\"max-height:128px; max-width:128px; min-height:128px; min-width: 128px\" ng-src=images/default-folder.svg alt={{deliver.title}}> </div> <p itemprop=name>{{deliver.title}}</p> </div> </div>"
  );


  $templateCache.put('views/amh-order.html',
    "<md-content layout-padding flex> <div ng-if=ctrl.loading layout=row style=\"height: 90vh\" layout-align=\"center center\"> <md-progress-circular md-mode=indeterminate md-diameter=96 layout-align=\"center center\"> </md-progress-circular> </div>  <div id=order-main-div layout=column layout-gt-md=row ng-if=!ctrl.loading>  <div id=order-left-div layout=column layout-align=\"start stretch\" flex=100 flex-gt-md=50 layout-margin>  <div id=order-info layout=row layout-xs=column layout-padding class=md-whiteframe-2dp> <table> <tr> <td translate=\"\">ID </td> <td>: {{ctrl.order.id}}</td> </tr> <tr ng-if=ctrl.order.secureId> <td translate=\"\">Secure id </td> <td>: {{ctrl.order.secureId}}</td> </tr> <tr class=noprint> <td translate=\"\">Title </td> <td>: {{ctrl.order.title}}</td> </tr> <tr> <td translate=\"\">Full name </td> <td>: {{ctrl.order.full_name}}</td> </tr> <tr> <td translate=\"\">Phone </td> <td>: {{ctrl.order.phone}}</td> </tr> <tr> <td translate=\"\">Address </td> <td>: {{ctrl.order.province}} - {{ctrl.order.city}} - {{ctrl.order.address}}</td> </tr> <tr class=noprint> <td translate=\"\">Description </td> <td>: {{ctrl.order.description}}</td> </tr> <tr class=noprint> <td translate=\"\">State </td> <td>: {{ctrl.order.state}}</td> </tr> </table> </div>  <div id=order-attachments class=\"md-whiteframe-2dp noprint\" layout-padding> <div layout=row layout-align=\"space-between center\"> <h2 translate=\"\">Attachments</h2> <md-button class=\"md-raised md-primary\" ng-click=ctrl.addAttachment() aria-label=\"Add attachment\"> <span translate=\"\">Add attachment</span> </md-button> </div> <md-list> <md-list-item ng-repeat=\"attachment in ctrl.order.attachments\" ng-click=ctrl.showImage(ctrl.order,attachment)> <img ng-if=\"attachment.mime_type.startsWith('image')\" class=md-avatar ng-src=/api/v2/shop/orders/{{ctrl.order.id}}/attachments/{{attachment.id}}/content ng-src-error=\"resources/images/baseline-help-24px.svg\"> <img ng-if=\"!attachment.mime_type.startsWith('image')\" class=md-avatar ng-src=\"resources/images/unknown-file.png\"> <p>{{attachment.description | limitTo:40}}</p> <md-button class=md-icon-button aria-label=\"Download attachment\" ng-href=/api/v2/shop/orders/{{ctrl.order.id}}/attachments/{{attachment.id}}/content target=_blank download={{attachment.file_name}}> <wb-icon>cloud_download</wb-icon> <md-tooltip><span translate=\"\">Download</span></md-tooltip> </md-button> <md-button class=md-icon-button aria-label=\"Delete attachment\" ng-click=ctrl.deleteAttachment(attachment)> <wb-icon>delete</wb-icon> <md-tooltip><span translate=\"\">Delete</span></md-tooltip> </md-button> </md-list-item> </md-list> </div>  <div id=order-items class=md-whiteframe-2dp layout-padding> <table align=center class=\"mb-table mb-shop-table\"> <thead> <tr md-colors=\"{color: 'primary-700'}\"> <td style=\"width: 5%\"></td> <td align=center translate=\"\" style=\"width: 60%\">Title</td> <td align=center translate=\"\" style=\"width: 10%\">Unit/Price</td> <td align=center translate=\"\" style=\"width: 10%\">Count</td> <td align=center translate=\"\" style=\"width: 10%\">Price</td> </tr> </thead> <tbody> <tr ng-repeat=\"item in ctrl.order.order_items track by $index\"> <td align=center> <wb-icon ng-if=\"item.item_type === 'service'\" class=noprint>cloud_upload</wb-icon> <wb-icon ng-if=\"item.item_type === 'product'\" class=noprint>add_shopping_cart</wb-icon> <wb-icon ng-if=\"item.item_type === 'deliver'\" class=noprint>send</wb-icon> </td> <td> <p>{{item.title}}</p> </td> <td align=center> <p>{{item.price | number}}</p> </td> <td align=center> <p>{{item.count}}</p> </td> <td align=center> <p>{{item.price * item.count | number}}</p> </td> </tr> <tr> <td style=\"width: 5%\"></td> <td align=start style=\"width: 60%\"> <h3 translate=\"\">Total price</h3> </td> <td style=\"width: 10%\"></td> <td style=\"width: 10%\"></td> <td ng-if=\"ctrl.order.order_items.length > 0\" align=center style=\"width: 10%\"> <h3>{{ctrl.totalP | number}}</h3> </td> </tr> </tbody> </table> </div>  <div ng-if=\"!ctrl.isPayed && ctrl.actionFlags.pay\" class=\"md-whiteframe-2dp noprint\" layout-padding> <mb-pay mb-pay=ctrl.pay($data) mb-discount-enable=true> </mb-pay> </div>  <div ng-if=\"ctrl.paymentChecked && ctrl.actionsChecked && (ctrl.isPayed || !ctrl.actionFlags.pay)\" class=\"md-whiteframe-2dp noprint\" style=\"background-color: green\" layout-padding> <h2 style=\"color: white;text-align: center\" translate=\"\">Order is payed successfully.</h2> </div> </div>  <div id=order-right-div class=noprint layout=column flex=100 flex-gt-md=50 layout-margin layout-align=\"start stretch\">  <md-list>  <md-list-item ng-if=ctrl.order.customer.id class=\"md-2-line md-whiteframe-2dp\"> <img class=md-avatar ng-src=/api/v2/user/accounts/{{ctrl.order.customer.id}}/avatar ng-src-error=\"https://www.gravatar.com/avatar/{{ctrl.order.customer.id| wbmd5}}?d=identicon&size=150\"> <div class=md-list-item-text layout=column> <h3>{{ctrl.order.customer.profiles[0].first_name}} {{ctrl.order.customer.profiles[0].last_name}}</h3> </div> </md-list-item>  <md-list-item ng-if=order.assignee class=\"md-2-line md-whiteframe-2dp\"> <img class=md-avatar ng-src=/api/v2/user/accounts/{{ctrl.order.assignee.id}}/avatar ng-src-error=\"https://www.gravatar.com/avatar/{{ctrl.order.assignee.id| wbmd5}}?d=identicon&size=150\"> <div class=md-list-item-text layout=column> <h3>{{ctrl.order.assignee.profiles[0].first_name}} {{ctrl.order.assignee.profiles[0].last_name}}</h3> </div> </md-list-item> </md-list>  <div layout=column layout-padding class=md-whiteframe-2dp> <md-list flex> <md-subheader><h2 style=\"color: black\">{{'Order history'| translate}}</h2></md-subheader> <md-list-item ng-repeat=\"history in ctrl.order.histories track by $index\" class=md-2-line style=\"cursor: pointer\"> <wb-icon ng-style=\"{'background-color': '#607D8B'}\" ng-if=\"history.action == 'create'\" class=md-avatar-icon alt={{history.action}}>add</wb-icon> <wb-icon ng-style=\"{'background-color': '#607D8B'}\" ng-if=\"history.action == 'accept'\" class=md-avatar-icon alt={{history.action}}>done</wb-icon> <wb-icon ng-style=\"{'background-color': '#607D8B'}\" ng-if=\"history.action == 'reject'\" class=md-avatar-icon alt={{history.action}}>clear</wb-icon> <wb-icon ng-style=\"{'background-color': '#607D8B'}\" ng-if=\"history.action == 'archive'\" class=md-avatar-icon alt={{history.action}}>drafts</wb-icon> <wb-icon ng-style=\"{'background-color': '#607D8B'}\" ng-if=\"history.action == 'done'\" class=md-avatar-icon alt={{history.action}}>done_all</wb-icon> <wb-icon ng-style=\"{'background-color': '#607D8B'}\" ng-if=\"history.action == 'pay'\" class=md-avatar-icon alt={{history.action}}>monetization_on</wb-icon> <div class=md-list-item-text layout=column> <h3>{{history.action}}</h3> <p>{{history.description}}</p> </div> <md-divider ng-if=\"$index < ctrl.order.histories.length - 1\"></md-divider> </md-list-item> </md-list> </div> </div> </div> </md-content>"
  );


  $templateCache.put('views/amh-product.html',
    "<md-content layout=column flex> <h1>Product</h1> <p>It is a product!</p> </md-content>"
  );


  $templateCache.put('views/amh-register-order.html',
    "<md-content flex> <div layout=column class=registerForm> <div ng-if=!ctrl.registeringOrder layout=column> <div layout=row layout-align=\"center center\" style=\"background-color: #660000;border-radius: 4px; box-shadow: 0 8px 10px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)\"> <h2 style=\"color: white;padding: 0px\" translate=\"\">Register order</h2> </div> <div layout=row style=\"background-color: white;box-shadow: 0 8px 10px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); border-radius: 4px;margin-top: 2px\"> <div flex layout-padding> <form name=regForm novalidate> <md-input-container class=\"md-block md-icon-float\"> <wb-icon>person</wb-icon> <input ng-model=ctrl.data.full_name name=fullname placeholder=\"{{'Full name'| translate}}\" class=inputfield required> <div ng-messages=regForm.fullname.$error> <div ng-message=required> <span translate=\"\">This field is required.</span> </div> </div> </md-input-container> <md-input-container class=\"md-block md-icon-float\"> <wb-icon>phone</wb-icon> <input ng-model=ctrl.data.phone name=phone class=\"form-control inputfield\" placeholder=\"{{'Phone'| translate}}\" required> <div ng-messages=regForm.phone.$error> <div ng-message=required> <span translate=\"\">This field is required.</span> </div> </div> </md-input-container> <md-input-container class=\"md-block md-icon-float\"> <wb-icon>email</wb-icon> <input ng-model=ctrl.data.email type=email name=email placeholder=\"{{'Email'| translate}}\" class=form-control required> <div ng-messages=regForm.email.$error> <div ng-message=required><span translate=\"\">This field is required.</span></div> </div> </md-input-container> <md-input-container class=\"md-block md-icon-float\"> <wb-icon>location_on</wb-icon> <input ng-model=ctrl.data.province placeholder=\"{{'Province'| translate}}\" class=inputfield> </md-input-container> <md-input-container class=\"md-block md-icon-float\"> <wb-icon>location_city</wb-icon> <input ng-model=ctrl.data.city placeholder=\"{{'City'| translate}}\" class=inputfield> </md-input-container> <md-input-container class=\"md-block md-icon-float\"> <wb-icon>home</wb-icon> <input ng-model=ctrl.data.address placeholder=\"{{'Address'| translate}}\" class=inputfield> </md-input-container> <md-input-container class=md-block> <label translate=\"\">Description</label> <textarea ng-model=ctrl.data.description md-select-on-focus class=inputfield></textarea> </md-input-container> <div layout=row layout-align=\"center center\"> <md-button class=\"md-raised md-accent\" ng-href=\"/\"> <span translate=\"\">Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" type=submit ng-click=ctrl.register(ctrl.data) ng-disabled=regForm.$invalid> <span translate=\"\">Register</span> </md-button> </div> </form> </div> </div> </div> <div layout=row layout-align=\"center center\" ng-if=ctrl.registeringOrder> <md-progress-circular md-mode=indeterminate md-diameter=96px> </md-progress-circular> </div> </div> </md-content>"
  );


  $templateCache.put('views/amh-service.html',
    "<md-content layout=column flex> <h1>Service</h1> <p>It is a service!</p> </md-content>"
  );


  $templateCache.put('views/dialogs/attachment-new.html',
    "<md-dialog flex=50 layout=column ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">Add attachment</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config._files) ng-disabled=myForm.$invalid> <wb-icon aria-label=\"Close dialog\">done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding>  <lf-ng-md-file-input lf-files=config._files accept=*/* progress preview drag flex> </lf-ng-md-file-input> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/dialogs/show-image.html',
    "<md-dialog flex=50 layout=column ng-cloak layout-align=\"center center\"> <md-toolbar> <div class=md-toolbar-tools> <h2 ng-if=\"config.attachment.mime_type.includes('image')\" translate=\"\"> {{config.attachment.description}} </h2> <h2 ng-if=\"!config.attachment.mime_type.includes('image')\" translate=\"\"> Unknown type </h2> <span flex></span> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding> <img ng-if=\"config.attachment.mime_type.includes('image')\" ng-src=/api/v2/shop/orders/{{config.order.id}}/attachments/{{config.attachment.id}}/content ng-src-error=\"src/resources/images/baseline-help-24px.svg\"> <img ng-if=\"!config.attachment.mime_type.includes('image')\" ng-src=\"src/resources/images/unknown-file.png\"> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/settings/amh-shop-setting-quick-access.html',
    "<md-input-container class=md-block> <wb-icon>search</wb-icon> <input ng-model=wbModel.categoryId type=number placeholder=\"Category id\"> </md-input-container>"
  );


  $templateCache.put('views/settings/amh-shop-setting-register-order.html',
    "<md-input-container class=md-block> <label translate=\"\">Order title</label> <input ng-model=ctrl.orderTitle ng-change=\"ctrl.setProperty('orderTitle', ctrl.orderTitle)\"> </md-input-container>"
  );


  $templateCache.put('views/widgets/amh-shop-category-quick-access.html',
    "<div mb-preloading=\"serviceLoading || working\"> <div ng-if=categories itemscope itemtype=http://schema.org/Thing> <md-nav-bar md-no-ink-bar=disableInkBar md-selected-nav-item=currentNavItem nav-bar-aria-label=\"navigation links\"> <md-nav-item ng-repeat=\"category in categories\" name={{category.id}} md-nav-click=loadServices(category) itemprop=name>{{category.name}} <meta ng-if=parentCategory.name itemprop=category content={{parentCategory.name}}> </md-nav-item> </md-nav-bar> </div> <div ng-if=services> <div layout-margin> <md-radio-group ng-model=selectedService> <md-radio-button ng-repeat=\"service in services.items\" ng-value=service class=\"md-primary am-shop-category-quick-option\"> <div itemscope itemtype=http://schema.org/Service flex layout=row> <span flex itemprop=name>{{service.title}}</span> <meta ng-if=parentOfService.name itemprop=category content={{parentOfService.name}}> <span>{{service.price | amhCurrency: unit}}</span> </div> </md-radio-button> </md-radio-group> </div>   <div layout=row layout-align=\"center center\"> <md-button class=\"md-primary md-raised\" ng-click=submitToOrder(selectedService) ng-disabled=!selectedService translate>Pay and Buy </md-button> </div> </div> <div ng-if=\"categoryNotFound || serviceNotFound\" layout=row layout-align=\"center center\"> <h2 style=\"color: red\" translate>Nothing found.</h2> </div> </div>"
  );


  $templateCache.put('views/widgets/amh-shop-register-form.html',
    "<md-content flex> <div ng-if=!ctrl.registeringOrder layout=column> <form name=regForm novalidate layout-padding style=\"background-color: white\"> <md-input-container class=\"md-block md-icon-float\"> <wb-icon>person</wb-icon> <input ng-model=ctrl.data.full_name name=fullname placeholder=\"{{'Full name'| translate}}\" class=inputfield required> <div ng-messages=regForm.fullname.$error> <div ng-message=required> <span translate=\"\">This field is required.</span> </div> </div> </md-input-container> <md-input-container class=\"md-block md-icon-float\"> <wb-icon>phone</wb-icon> <input ng-model=ctrl.data.phone name=phone class=\"form-control inputfield\" placeholder=\"{{'Phone'| translate}}\" required> <div ng-messages=regForm.phone.$error> <div ng-message=required> <span translate=\"\">This field is required.</span> </div> </div> </md-input-container> <md-input-container class=\"md-block md-icon-float\"> <wb-icon>email</wb-icon> <input ng-model=ctrl.data.email type=email name=email placeholder=\"{{'Email'| translate}}\" class=form-control required> <div ng-messages=regForm.email.$error> <div ng-message=required><span translate=\"\">This field is required.</span></div> </div> </md-input-container> <md-input-container class=\"md-block md-icon-float\"> <wb-icon>location_on</wb-icon> <input ng-model=ctrl.data.province placeholder=\"{{'Province'| translate}}\" class=inputfield> </md-input-container> <md-input-container class=\"md-block md-icon-float\"> <wb-icon>location_city</wb-icon> <input ng-model=ctrl.data.city placeholder=\"{{'City'| translate}}\" class=inputfield> </md-input-container> <md-input-container class=\"md-block md-icon-float\"> <wb-icon>home</wb-icon> <input ng-model=ctrl.data.address placeholder=\"{{'Address'| translate}}\" class=inputfield> </md-input-container> <md-input-container class=md-block> <label translate=\"\">Description</label> <textarea ng-model=ctrl.data.description md-select-on-focus class=inputfield></textarea> </md-input-container> <div layout=row layout-align=\"center center\"> <md-button class=\"md-raised md-accent\" ng-href=\"/\"> <span translate=\"\">Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" type=submit ng-click=ctrl.register(ctrl.data) ng-disabled=regForm.$invalid> <span translate=\"\">Register</span> </md-button> </div> </form> </div> <div layout=row layout-align=\"center center\" ng-if=ctrl.registeringOrder> <md-progress-circular md-mode=indeterminate md-diameter=96px> </md-progress-circular> </div> </md-content>"
  );


  $templateCache.put('views/widgets/amh-shop-track-order.html',
    "<div layout-padding> <div layout=row layout-align=\"start center\"> <md-input-container class=\"md-icon-float md-icon-right md-block\" flex> <label translate=\"\">Secure id</label> <input ng-model=orderSecureId> </md-input-container> <md-button class=\"md-raised md-primary\" aria-label=Search ng-disabled=editable ng-click=orderFinder(orderSecureId)> <span translate=\"\">Search</span> </md-button> </div> </div>"
  );

}]);
